var Schedule = {
	init : function() {
		$.ajax({
			type : 'GET',
			url : 'http://mark.koodidojo.com/api/v1/users/' + App.user.id + '/subjects',
			dataType : 'json',
			beforeSend : function() {
	  			$('#main-content').append(App.loader());
			},
			success : function(data) {
				console.log(data);
				if (data.error) {
					return false;
				}

				App.loadView('schedule', data);
			}
		});

	}
};

$(document).ready(function() {
	Schedule.init();
});
