var Load = {
	DOMListeners : {
		init : false
	},

	init : function() {
		var self = this;

		App.loadView('load', {});

		if (!self.DOMListeners.init) {
			$(document).on('click', '.redeem-load', function() {
				var code = $('input[name="load-code"]').val();

				if ($.isEmptyObject(code)) {
					alert('Code must not be empty.');
					return false;
				}

				$.ajax({
					url : 'http://mark.koodidojo.com/api/v1/load/'+code+'/redeem',
					dataType: 'json',
					beforeSend : function() {
						$('#main-content').append(App.loader());
					},
					success : function(data) {
						console.log(data);
						$('.loader').remove();
						if (!data.error) {
							App.loadScript('news');
						}
					}
				});
			});

			self.DOMListeners.init = true;
		}
	}
};

$(document).ready(function() {
	Load.init();
});
