var More = {
	init : function() {		
		console.log(App.user);
		// compile partial
		App.getPartial('more', function(partial) {
			var template 	= Handlebars.compile(partial);
			var html 		= template(App.user);

			$('#main-content').html(html);
		});
	}
};

$(document).ready(function() {
	More.init();
});