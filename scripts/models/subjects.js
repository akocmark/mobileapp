var Subjects = {
	DOMListeners : {
		show : false,
		tab : {
			addStudent : false,
			searchstudent : false
		}
	},

	init : function() {
		var self = this;

		if (!$.isEmptyObject(App.$_GET) && !$.isEmptyObject(App.$_GET.id)) {
			if (!$.isEmptyObject(App.$_GET.tab)) {
				return self.tab[App.$_GET.tab](App.$_GET.id);
			}

			return self.show(App.$_GET.id);
		}

		self.getSubjects(App.user.id, function(data) {

			var context = {
				user : App.user,
				subjects : data.subjects
			};

			App.loadView('subjects', context);
		})
	},

	all : function() {

	},

	show : function(id) {
		var self = this;
		
		App.loadScript('jquery.timeago.min', true);

		var url = 'http://mark.koodidojo.com/api/v1/subjects/' + id;
		$.ajax({
			url : url,
			dataType : 'json',
			beforeSend : function() {
	  			$('#main-content').append(App.loader());
			},
			success : function(data) {
				if (!data.error) {
					data.user = App.user;
					console.log(data);

					App.loadView('subjects.view', data, self.minifyPosts);
				}
			}
		});

		// return, if dom listeners are already loaded
		if (self.DOMListeners.show) {
			return true;
		}

		$(document).on('click', '.see-more', function() {
			$(this).prev().height('auto');
			$(this).prev().css('overflow', 'visible');
			$(this).remove();
		});

		//add new post
		$(document).on('click', '.subject-new-post', function() {
			var _this = this;
			var postVal = $('textarea.input-post');

			if ($.isEmptyObject(postVal.val())) {
				return false;
			}

			$.ajax({
				type : 'POST',
				url : 'http://mark.koodidojo.com/api/v1/subjects/' + id + '/posts',
				data : {
					subject_id : App.$_GET.id,
					post : postVal.val()
				},
				dataType : 'json',
				beforeSend : function() {
					$(_this).addClass('disabled');
				},
				success : function(data) {
					if (!data.error) {
						var context = {posts : [data.post]};

						context.posts[0].user = {
							name : App.user.name,
							role_id : App.user.role_id,
							photo : App.user.photo
						},

						App.getPartial('posts', function(template) {
							var template = Handlebars.compile(template);
							var html = template(context);

							$('.posts').prepend(html);
							self.minifyPosts();

							$(_this).removeClass('disabled');
							postVal.val('');
						});
					}
				}
			});

			return false;
		});

		//delete post
		$(document).on('click', '.delete-post', function() {
			if (!confirm('Are you sure you want to delete this post?')) {
				return false;
			}

			var _this = this;
			$.ajax({
				type : 'DELETE',
				url : 'http://mark.koodidojo.com/api/v1/posts/' + $(_this).data('id'),
	  			beforeSend : function() {
	  				$('#main-content').append(App.loader('Deleting...'))
	  			},
				success : function(data) {
					if (!data.error) {
						console.log(data);
						$('.loader').remove();
						console.log($(_this).parent().parent().remove());
					}
				}
			});
		});

		self.DOMListeners.show = true;
	},

	tab : {
		info : function(id) {
			$.ajax({
				url : 'http://mark.koodidojo.com/api/v1/subjects/' + id + '/info',
				beforeSend : function() {
		  			$('#main-content').append(App.loader('Getting subject info...'));
				},
				success : function(data) {
					var context = {	
						subject : data.subject,
						user : App.user
					};
					console.log(context);
					App.loadView('subjects.info', context);
				}
			});
		},
		students : function(id) {
			$.ajax({
				url : 'http://mark.koodidojo.com/api/v1/subjects/' + id + '/students',
				beforeSend : function() {
		  			$('#main-content').append(App.loader('Getting students...'));
				},
				success : function(data) {
					console.log(data);
					App.loadView('subjects.students', data);
				}
			});
		},
		searchstudent : function(id) {
			if (!$.isEmptyObject(App.$_GET.query)) {
				$.ajax({
					url : 'http://mark.koodidojo.com/api/v1/users/search/' + App.$_GET.query,
					beforeSend : function() {
		  				$('#main-content').append(App.loader('Searching students...'));
					},
					success : function(data) {
						console.log(data);
						$('.loader').remove();

						if (data.error) {
							return false;
						}

						App.loadView('subjects.searchstudent', data, function() {
							$('input[name="student-name"]').val(App.$_GET.query);
						}, true);

					}
				});
			} else {
				App.loadView('subjects.searchstudent', {});
			}

			if (window.Subjects.DOMListeners.tab.searchstudent) {
				return true;
			}

			$(document).on('click', '.subject-addstudent .search', function() {
				var script = 'subjects?id=21&tab=searchstudent&query=' + $('input[name="student-name"]').val();
				App.loadScript(script);
			});

			$(document).on('click', '.add-student', function() {
				var _this = this;

				$.ajax({
					type : 'POST',
					url : 'http://mark.koodidojo.com/api/v1/subjects/' + App.$_GET.id + '/students',
					data : {
						user_id : $(_this).data('id')
					},
					dataType : 'json',
					beforeSend : function() {
						$(_this).addClass('disabled');
					},
					success : function(data) {
						if (data.error) {
							return false;
						}

						$(_this).text('Added');
					}
				});
			});


			window.Subjects.DOMListeners.tab.searchstudent = true;
		}
	},

	getSubjects : function(userId, callback) {
		$.ajax({
			type : 'GET',
			url : 'http://mark.koodidojo.com/api/v1/users/' + userId + '/subjects',
			dataType : 'json',
			beforeSend : function() {
	  			$('#main-content').append(App.loader());
			},
			success : function(data) {
				console.log(data);
				callback(data);
			}
		});
	},

	minifyPosts : function() {
		$('.post-date').timeago();
		

		$('.post-body').each(function(k, v) {
			var style = function(){};

			if ($(v).height() > 200) {
				style = function() {
					$(v).height('200px');
					$(v).css('overflow', 'hidden');
					$('<a href="" class="col-xs-12 see-more">See More</a>').insertAfter(v);
				};
			}

			if ($(v).height() > 400) {
				style = function() {
					$(v).height('200px');
					$(v).css('overflow', 'hidden');
					$('<a href="#posts?id='+$(v).data('id')+'" class="col-xs-12">Continue Reading</a>').insertAfter(v);
				};
			}

			style();
		});
	}
};

$(document).ready(function() {
	Subjects.init();
});
