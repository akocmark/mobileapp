var Dashboard = {

	init : function() {
		var self = this;
		App.loadView('dashboard', App.user, function() {
			App.headerLoadOut(false);
		});
	}
};

$(document).ready(function() {
	Dashboard.init();
});
