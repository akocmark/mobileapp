var Grades = {
	DOMListeners : {
		addrule : false
	},

	init : function() {
		var self = this;

		
		if (!$.isEmptyObject(App.$_GET)) {
			if (!$.isEmptyObject(App.$_GET.subject)) {
				// student
				if (App.user.role.name == 'student') {
					return self.getMyGrade(App.user.id);
				}

				// action
				if (!$.isEmptyObject(App.$_GET.action)) {
					return self[App.$_GET.action]();
				}

				// input grades
				if (!$.isEmptyObject(App.$_GET.user)) {
					return self.inputGrades();
				}

				return self.studentList(App.$_GET.subject);
			}
		}

		self.getSubjects(function(data) {
			App.loadView('grades', data);
		});
	},

	getSubjects : function(callback) {
		$.ajax({
			type : 'GET',
			url : 'http://mark.koodidojo.com/api/v1/users/' + App.user.id + '/subjects',
			dataType : 'json',
			beforeSend : function() {
	  			$('#main-content').append(App.loader());
			},
			success : function(data) {
				console.log(data);
				callback(data);
			}
		});
	},

	studentList : function(subject_id) {
		$.ajax({
			url : 'http://mark.koodidojo.com/api/v1/subjects/' + subject_id + '/students',
			beforeSend : function() {
	  			$('#main-content').append(App.loader('Getting students...'));
			},
			success : function(data) {
				var context = {
					subjectId : App.$_GET.subject,
					students : data.students,
				};
				console.log(context);
				App.loadView('subjects.students', context);
			}
		});
	},

	inputGrades : function() {
		var self = this;
		var allowEmpty = false;

		self.getGradingRule(allowEmpty, function(gradingRule) {
			var context = {};
			context.rules = gradingRule.rules;

			self.getUserGrade(App.$_GET.user, function(userGrades) {
				// merge context
				$.extend(context, userGrades);
				console.log(context);
				context.average = self.computeAverage(context);
		
				console.log(context);
				App.loadView('grades.add', context, null, true);
			});
		});

		// dom listeners
		if (!self.DOMListeners.inputGrades) {

			$(document).on('click', '.save-grade', function() {
				var _this = this;
				var data = {
					user_id : App.$_GET.user,
					subject_id : App.$_GET.subject,
					grades : {}
				};

				var err=false, msg='';
				$('input').each(function(k, v) {
					var val = $(v).val();

					if ( !$.isEmptyObject(val) ) {
						val = Number(val);

						if (val > 100) {
							err = true;
							msg += $(v).data('criteria') + " must not be above 100.\n";

							return true;
						}

						data.grades[$(v).data('id')] = val;
					}
				});

				if (err) {
					alert(msg);
					return false;
				}


				$.ajax({
					type : 'POST',
					url : 'http://mark.koodidojo.com/api/v1/users/'+App.$_GET.user+'/savegrades',
					dataType : 'json',
					data : data,
					beforeSend : function() {
						$(_this).addClass('disabled');
					},
					success : function(data) {
						$(_this).removeClass('disabled');
						console.log(data);
						App.loadScript('grades?subject='+App.$_GET.subject+'&user='+App.$_GET.user);
					}
				});
			});

			self.DOMListeners.inputGrades = true;
		}
	},

	getMyGrade : function(user_id) {
		var self 		= this;
		var allowEmpty 	= false;

		self.getGradingRule(allowEmpty, function(gradingRule) {
			gradingRule = gradingRule.error ? {rules:[]} : gradingRule;
			
			var context = {};
			context.rules = gradingRule.rules;

			self.getUserGrade(user_id, function(userGrades) {
				// merge context
				$.extend(context, userGrades);
				console.log(context);
				context.average = self.computeAverage(context);
		
				console.log(context);
				App.loadView('mygrades', context);
			});
		}, true);
	},

	getUserGrade : function(user_id, callback) {
		var self = this;

		$.ajax({
			url : 'http://mark.koodidojo.com/api/v1/users/'+user_id+'/grades',
			dataType : 'json',
			beforeSend : function() {
				$('#main-content').append(App.loader());
			},
			success : function(data) {
				callback(data);
			}
		});
	},

	computeAverage : function(context) {
		var average = 0;

		$.each(context.rules, function(k, v) {
			if (context.grades.hasOwnProperty(v.id)) {
				average += context.grades[v.id].value * (v.percent/100);
			}
		});

		return average;
	},

	getGradingRule : function(allowEmpty, callback, fromStudent) {
		console.log('getrule');
		var subject_id = App.$_GET.subject;

		$.ajax({
			url : 'http://mark.koodidojo.com/api/v1/subjects/'+subject_id+'/gradingrule',
			beforeSend : function() {
				$('#main-content').append(App.loader());
			},
			data : {
				allowEmpty : allowEmpty
			},
			dataType : 'json',
			success : function(data) {
				console.log(data);
				$('.loader').remove();

				if (data.empty && !allowEmpty && !fromStudent) {
					return App.loadScript('grades?subject='+subject_id+'&action=addrule');
				}

				callback(data);
			}

		});
	},

	addrule : function() {
		var self = this;
		var allowEmpty = true;

		self.getGradingRule(allowEmpty, function(data) {
			App.loadView('grades.addrule', data, null, true);
		})


		// DOM listeners
		if (!self.DOMListeners.addrule) {
			$(document).on('click', '.save-rule', function() {
				var _this = this;
				var criteria = $('input[name="grade-criteria"]').val();
				var percent = $('input[name="grade-percent"]').val();

				if ($.isEmptyObject(criteria)) {
					alert('Criteria can\'t be empty');
					return false;
				}

				if ($.isEmptyObject(percent)) {
					alert('Percent can\'t be empty');
					return false;
				}

				if (!$.isNumeric(percent)) {
					alert('Percent must be a number');
					return false;
				}

				$.ajax({
					type : 'POST',
					url : 'http://mark.koodidojo.com/api/v1/subjects/'+App.$_GET.subject+'/gradingrule',
					data : {
						criteria : criteria,
						percent : percent
					},
					dataType : 'json',
					beforeSend : function() {
						$(_this).addClass('disabled');
					},
					success : function(data) {
						$(_this).removeClass('disabled');
						App.loadScript('grades?subject='+App.$_GET.subject+'&action=addrule');
					}
				});
			});
			self.DOMListeners.addrule = true;
		}
	}
};

$(document).ready(function() {
	Grades.init();
});
