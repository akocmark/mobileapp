var News = {
	DOMListeners : {
		init : false,
		addnews : false,
		show : false
	},

	page : null,

	init : function() {
		var self 	= this;
		self.page 	= 1;

		if (!$.isEmptyObject(App.$_GET)) {
			if (!$.isEmptyObject(App.$_GET.action)) {
				return self[App.$_GET.action]();
			}

			if (!$.isEmptyObject(App.$_GET.id)) {
				return self.show(App.$_GET.id)	;
			}
		}

		App.loadScript('jquery.timeago.min', true);

		// fetch posts on the server
		self.fetchPosts(self.page, function(data) {
			$.ajax({
				url : 'http://mark.koodidojo.com/api/v1/users/userload',
				dataType : 'json',
				success : function(loadData) {
					var context = {
						news : data.news,
						user : App.user,
						accountLoad : loadData.load
					};
					console.log(context);
					
					//load view
					App.loadView('news', context, self.minifyNews);

					App.headerLoadOut(false);
				}
			});
		});


		// autload posts on scroll
		self.postAutoload();

		if (!self.DOMListeners.init) {
			//delete news
			$(document).on('click', '.delete-news', function() {
				if (!confirm('Are you sure you want to delete this news?')) {
					return false;
				}

				var _this = this;
				$.ajax({
					type : 'DELETE',
					url : 'http://mark.koodidojo.com/api/v1/news/' + $(_this).data('id'),
		  			beforeSend : function() {
		  				$('#main-content').append(App.loader('Deleting...'))
		  			},
					success : function(data) {
						console.log(data);
						if (!data.error) {
							$('.loader').remove();
							$(_this).parent().parent().remove();
						}
					}
				});
			});
			self.DOMListeners.init = true;
		}
	},

	addnews : function() {
		var self = this;

		App.loadView('news.add', {});

		if (self.DOMListeners.addnews) {
			return true;
		}

		$(document).on('click', '.publish-news', function() {
			var _this = this;

			var newsTitle = $('input[name="news-title"]').val();
			var newsBody = $('textarea[name="news-body"]').val();

			if ($.isEmptyObject(newsTitle)) {
				alert('News title can\'t be empty.');
				return false;
			}

			if ($.isEmptyObject(newsBody)) {
				alert('News body can\'t be empty.');
				return false;
			}

			$.ajax({
				type : 'POST',
				url : 'http://mark.koodidojo.com/api/v1/news',
				data : {
					title : newsTitle,
					body : newsBody
				},
				dataType : 'json',
				beforeSend : function() {
					$(_this).addClass('disabled');
				},
				success : function(data) {
					App.loadScript('news', {}, null, true);
				}
			});
		});

		self.DOMListeners.addnews = true;
	},

	postAutoload : function() {
		var self 			= this;
	  	var allowAutoload 	= true;

		$(window).scroll(function() {
			if (App.getHash() !== '#news') {
				return false;
			}

		    if (allowAutoload && ($(window).scrollTop() >  $(document).height() - $(window).height() - 400)) {
		        allowAutoload = false;

		        // autoload content
				App.getPartial('news', function(partial) {
		  			self.page++;

					// fetch posts via on the server
					self.fetchPosts(self.page, function(data) {
						var context = {
							news : data.news,
							user : App.user
						};
						console.log(context);
						// compile partial
						var template 	= Handlebars.compile(partial);
						var html 		= template(context);
						
						//append posts
						$('.newss').append(html);
						self.minifyNews();

						// reset allowAutoload flag if posts count is >= 20
						if (data.news.length >= 8) {
							allowAutoload = true;
						} else {
	       					$('.newss').append('<div class="news-loader" style="text-align: center">No more news to load.</div>');
						}
					});
				});
		    }
		});
	},

	fetchPosts : function(page, callback) {
		$.ajax({
	    	type : 'GET',
	    	data : {
	    		page: page
	    	},
	  		url : "http://mark.koodidojo.com/api/v1/news",
	  		beforeSend : function() {
				$('.newss').append('<div class="news-loader col-xs-12" style="text-align: center">Loading..</div>');
	  		},
	  		success : function(data) {
	  			$('.news-loader').remove();
	  			console.log(data);
	  			callback(data);
	  		}
	  	});
	},

	minifyNews : function() {
		$('.news-date').timeago();
		

		$('.news-body').each(function(k, v) {
			var style = function(){};

			if ($(v).height() > 200) {
				style = function() {
					$(v).height('200px');
					$(v).css('overflow', 'hidden');
					$('<a href="" class="col-xs-12 see-more">See More</a>').insertAfter(v);
				};
			}

			if ($(v).height() > 400) {
				style = function() {
					$(v).height('200px');
					$(v).css('overflow', 'hidden');
					$('<a href="#news?id='+$(v).data('id')+'" class="col-xs-12">Continue Reading</a>').insertAfter(v);
				};
			}

			style();
		});
	},

	show : function(id) {
		var self = this;

		App.loadScript('jquery.timeago.min', true);

		$.ajax({
			url : 'http://mark.koodidojo.com/api/v1/news/' + id,
			beforeSend : function() {
	  			$('#main-content').append(App.loader());
			},
			success : function(data) {
				App.loadView('news.view', data, function () {
					$('.news-date').timeago();
				});
			}
		});

		// return, if dom listeners are already loaded
		if (self.DOMListeners.show) {
			return true;
		}

		// DOM listeners here

		self.DOMListeners.show = true;
	}
};

$(document).ready(function() {
	News.init();
});
