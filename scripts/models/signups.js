var Signups = {
	DOMListeners : {
		signups : false,
	},

	init : function() {
		var self = this;
		$.ajax({
			url : 'http://mark.koodidojo.com/api/v1/users/signups',
			beforeSend : function() {
				$('#main-content').append(App.loader());
			},
			success : function(data) {
				console.log(data);
				App.loadView('signups', data);
			}
		});

		if (self.DOMListeners.signups) {
			return true;
		}

		$(document).on('click', '.confirm-student', function() {
			var _this = this;

			$.ajax({
				url : 'http://mark.koodidojo.com/api/v1/users/' + $(_this).data('id') + '/activate',
				beforeSend : function() {
					$('#main-content').append(App.loader('Confirming students...'));
					$(_this).addClass('disabled');
				},
				success : function(data) {
					console.log(data);
					$('.loader').remove();
					$(_this).text('Cofirmed');

					if (data.error) {
						return true;
					}
				}
			});
		});

		self.DOMListeners.signups = true;
	}
};

$(document).ready(function() {
	Signups.init();
});
