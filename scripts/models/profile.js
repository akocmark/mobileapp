var Profile = {
	init : function() {
		var self = this;

		App.loadScript('jquery.timeago.min', true);

		$.ajax({
			url : 'http://mark.koodidojo.com/api/v1/users/' + App.user.id + '/stories',
			beforeSend : function() {
				$('#main-content').append(App.loader());
			},
			success : function(data) {
				if (data.error) {
					return true;
				}

				var context = {
					user : App.user,
					stories : data.stories
				};

				App.loadView('profile', context, function() {
					$('.post-date').timeago();

					$('.subject-teaser').each(function(k, v) {
						var style = function(){};

						if ($(v).height() > 150) {
							$(v).height('150px');
							$(v).css('overflow', 'hidden');
							$(v).css('margin-top', '20px');
						}
					});
				});
			}
		});
	},
};

$(document).ready(function() {
	Profile.init();
});