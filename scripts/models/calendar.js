var Calendar = {
	DOMListeners : {
		init : false,
		addevent : false
	},

	init : function() {
		var self = this;

		if (!$.isEmptyObject(App.$_GET) && !$.isEmptyObject(App.$_GET.action)) {
			return self[App.$_GET.action]();
		}

		App.loadView('calendar', {user : App.user}, self.loadCalendar(), true);

		App.headerLoadOut(false);

		if (self.DOMListeners.init) {
			return true;
		}

		$(document).on('change', 'select[name="year"]', function() {
			var y = $(this).val();
			var m = $('select[name="month"]').val();
			App.loadScript('calendar?month='+m+'&year='+y);
		});

		$(document).on('change', 'select[name="month"]', function() {
			var m = $(this).val();
			var y = $('select[name="year"]').val();
			App.loadScript('calendar?month='+m+'&year='+y);
		});

		self.DOMListeners.init = true;
	},

	addevent : function() {
		var self = this;

		App.loadView('calendar.add', {}, function() {
			$('input[name="event-date"]').pickadate({
    			format: 'yyyy-mm-dd'
			});
		});

		if (self.DOMListeners.addevent) {
			return true;	
		}

		$(document).on('click', '.post-new-event', function() {
			var _this = this;

			var eventName = $('input[name="event-name"]').val();
			var eventDesc = $('textarea[name="event-description"]').val();
			var eventDate = $('input[name="event-date"]').val();

			if ($.isEmptyObject(eventName)) {
				alert('Event name can\'t be empty.');
				return false;
			}

			if ($.isEmptyObject(eventDesc)) {
				alert('Event desc can\'t be empty.');
				return false;
			}

			if ($.isEmptyObject(eventDate)) {
				alert('Event date can\'t be empty.');
				return false;
			}

			$.ajax({
				type : 'POST',
				url : 'http://mark.koodidojo.com/api/v1/events',
				beforeSend : function() {
					$('#main-content').append(App.loader('Saving event...'));
				},
				data : {
					name : eventName,
					desc : eventDesc,
					date : eventDate
				},
				dataType : 'json',
				success : function(data) {
					console.log(data);
					if (data.error) {
						return false;
					}

					App.loadScript('calendar');
				}
			});
		});

		self.DOMListeners.addevent = true;
	},

	loadCalendar : function() {
		var self = this;

		return function() {
			var date = new Date();
			if (!$.isEmptyObject(App.$_GET) && !$.isEmptyObject(App.$_GET.month)) {
				var m = App.$_GET.month;
				var y = $.isEmptyObject(App.$_GET.year) ? date.getFullYear() : App.$_GET.year;

				date = new Date(y, m, 1);
			}

			// get events from the server
			$.ajax({
				url : 'http://mark.koodidojo.com/api/v1/events/month/' + (date.getMonth() + 1),
				beforeSend : function() {
					$('#main-content').append(App.loader());
				},
				success : function(data) {
					console.log(data);
					$('.loader').remove();

					$('#main-content').append(
						'<div class="row calendar-wrapper">' +
						self.draw_calendar(date.getMonth(), date.getFullYear(), data.events) +
						'</div>'
					);
				}
			});

			var years = [date.getFullYear()];
			for (var i = 1; i <= 2; i++) {
				years.push(date.getFullYear() - i);
				years.push(date.getFullYear() + i);
			};
			years = years.sort();

			var options = '';
			$.each(years, function(k, v) {
				options += '<option value="'+v+'">'+v+'</option>';
			});

			$('select[name="year"]').html(options);
			$('select[name="year"]').val(date.getFullYear());
			console.log(date);
			$('select[name="month"]').val(date.getMonth());
		}
	},

	draw_calendar : function(month, year, events) {
		var date;

		var month_today;
		var year_today;

		/* draw table */
		var calendar = '<table class="calendar" cellpadding="0" cellspacing="1">';

		/* table headings */
		var headings = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
		calendar += '<tr class="calendar-row"><td class="calendar-day-head">' +headings.join('</td><td class="calendar-day-head">')+ '</td></tr>';

		/* days and weeks vars now ... */
		var running_day = new Date(year, month, 1).getDay();

		var monthStart = new Date(year, month, 1);
		var monthEnd = new Date(year, month + 1, 1);
		var days_in_month = (monthEnd - monthStart) / (1000 * 60 * 60 * 24)  

		var days_in_this_week = 1;
		var day_counter = 0;
		var dates_array = new Array();

		/* row for week one */
		calendar += '<tr class="calendar-row">';

		/* print "blank" days until the first of the current week */
		for(var x = 0; x < running_day; x++) {
			calendar += '<td class="calendar-day-np"> </td>';
			days_in_this_week++;
		}

		/* keep going with days.... */
		for(var list_day = 1; list_day <= days_in_month; list_day++) {
			var day_count = '&nbsp;';
			var nice_month = month_today+1;
			var zxcasd = year_today + '-' + nice_month + '-' + list_day;
			var hasEvent = false;
			var classs = '';

			if (events.hasOwnProperty(list_day)) {
				hasEvent = true;
				classs = 'has-event'
			}

			calendar += '<td class="calendar-day '+classs+'" onclick="single_view(\''+zxcasd+'\')">';
				/* add in the day number */
				calendar += '<div class="day-number">' +list_day+ '</div>';
				/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
				if (hasEvent) {
					calendar += '<div class="" style="height: 50px; font-size: 10px; overflow: hidden; word-break: break-all;">'+events[list_day].name+'</div>';	
				} else {
					calendar += '<div class="whole-filler"></div>';	
				}
			calendar += '</td>';

			if(running_day == 6) {
				calendar += '</tr>';
				if((day_counter+1) != days_in_month){
					calendar += '<tr class="calendar-row">';
				}
				running_day = -1;
				days_in_this_week = 0;
			}

			days_in_this_week++; running_day++; day_counter++;
		}

		/* finish the rest of the days in the week */
		if(days_in_this_week < 8){
			for(var x = 1; x <= (8 - days_in_this_week); x++) {
				calendar += '<td class="calendar-day-np"> </td>';
			}
		}

		/* final row */
		calendar += '</tr>';

		/* end the table */
		calendar += '</table>';
		
		/* all done, return result */
		return calendar;
	} 
};

$(document).ready(function() {
	Calendar.init();
});
