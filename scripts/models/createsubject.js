var CreateSubject = {
	init : function() {
		App.loadView('createsubject', App.user);

		$(document).on('submit', '#form-create-subject', function(e) {
			e.preventDefault();

			$.ajax({
	            type : 'POST', 
	            url  : 'http://mark.koodidojo.com/api/v1/subjects',
	            data : {
	            	code : $('input[name="subject-code"]').val(),
	            	name : $('input[name="subject-name"]').val(),
	            	schedule : $('input[name="schedule"]').val(),
	            	description : $('textarea[name="description"]').val(),
	            	user_id : App.user.id
	            },
	            dataType : 'json',
	            success : function(data) {
	            	console.log(data);
	            	App.loadScript('subjects');
	            }
	        })
		});
	}
};

$(document).ready(function() {
	CreateSubject.init();
});