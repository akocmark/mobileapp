var Posts = {
	DOMListeners : {
		show : false
	},

	init : function() {
		var self = this;

		App.loadScript('jquery.timeago.min', true);

		if (!$.isEmptyObject(App.$_GET) && !$.isEmptyObject(App.$_GET.id)) {
			return self.show(App.$_GET.id);
		}
	},

	show : function(id) {
		var self = this;

		$.ajax({
			url : 'http://mark.koodidojo.com/api/v1/posts/' + id,
			beforeSend : function() {
	  			$('#main-content').append(App.loader());
			},
			success : function(data) {
				console.log(data);

				App.loadView('posts.view', data, function () {
					$('.post-date').timeago();
				});
			}
		});

		// return, if dom listeners are already loaded
		if (self.DOMListeners.show) {
			return true;
		}

		self.DOMListeners.show = true;
	},
};

$(document).ready(function() {
	Posts.init();
});
