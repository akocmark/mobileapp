var App = {
	hash : null,

	user : {},

	$_GET : {},

	init : function() {
		var self = this;

		// get user if previously logged in
		var user = JSON.parse(localStorage.getItem("USER"));
		if (!$.isEmptyObject(user)) {
			user.isStudent = user.role.name == 'student' ? true : false;
			self.user =  user;
		}

		// App ajax event listeners
		self.ajaxGlobalSetup();
		self.ajaxSend();
		self.ajaxSuccess();

		// onpopstate listener
		self.onpopstate();
		
		// stop here on login page
		if (window.location.pathname == '/login.html' || window.location.pathname == '/register.html') {
			return false;
		}

		// if not login
		if (!self.isLogin()) {
			window.location.replace('/login.html');
		}

		// register partials to be used
		App.registerPartials(['header']);
		App.registerPartials(['posts']);
		App.registerPartials(['news']);

		// get the url hash and load the javascript for the hash
		var script 	= self.getHash();
		script 		= script.substr(1);
		self.loadScript(script);

		// override anchor tags behaviour
		$(document).on('click', 'a', function() {
			var script = $(this).attr('href').substr(1);

			if ($.isEmptyObject(script)) {
				return false;
			}

			// same state
			if (script == self.getHash().substr(1)) {
				return false;
			}

			self.loadScript(script);
			return false;
		});

		self.handlebarsHelpersInit();
	},

	getHash : function() {
		return !$.isEmptyObject(window.location.hash) ? window.location.hash : '#dashboard';
	},
	
	isLogin : function() {
		var self = this;
		if (!$.isEmptyObject(self.user.session_id)) {
			return true;
		}

		return false;
	},

	login : function(email, password) {
		var self = this;
		$.ajax({
	    	type : 'POST',
	    	dataType: 'json',
	  		url : "http://mark.koodidojo.com/api/login",
	  		data : {
	  			email: email,
	  			password: password
	  		},
	  		success : function(data) {
	  			if(!data.error) {
	  				this.user = data.user;
					localStorage.setItem("USER", JSON.stringify(data.user));

					window.location.href = '/';
				}

	  		},

	  		error : function(a,b,c) {
	  			alert('Unable to login.');
	  		}
	  	});

		return;
	},

	signup : function(name, email, password, course, yearsection) {
		var self 	= this;
		var err 	= false;
		var errMsg  = '';
		var args 	= ['Name', 'Email', 'Password', 'Course', 'Yearsection'];

		if ($.isEmptyObject(name)) {
			alert('Full name is required');
			return false;
		}
		if ($.isEmptyObject(email)) {
			alert('Email is required');
			return false;
		}
		if (!validEmail(email)) {
			alert('Email is not valid');
			return false;
		}
		if ($.isEmptyObject(password)) {
			alert('Password is required');
			return false;
		}
		if ($.isEmptyObject(course)) {
			alert('Course is required');
			return false;
		}
		if ($.isEmptyObject(yearsection)) {
			alert('Year and section is required');
			return false;
		}

		$.each(arguments, function(k, v) {
			if (args[k] == 'Course' || args[k] == 'Yearsection') {
				return true;
			}

			if (v.length < 6) {
				errMsg += args[k] + " must be atleast 5 characters.\n";
				err = true;
			}
		});

		if (err) {
			alert(errMsg);
			return false;
		}


		$.ajax({
	    	type : 'POST',
	    	dataType: 'json',
	  		url : "http://mark.koodidojo.com/api/register",
	  		data : {
	  			name: name,
	  			email: email,
	  			password: password,
	  			course: course,
	  			yearsection: yearsection
	  		},
	  		success : function(data) {
	  			console.log(data);
	  			if(!data.error) {
	  				alert('Success! Please wait for your account to be verified by a staff.');
				}

	  		},
	  		error : function(a,b,c) {
	  			alert('Unable to register at the moment.');
	  		}
	  	});

		return;
	},

	confirmLogout : function() {
		if (confirm('Are you sure you want to logout?')) {
			App.logout();
		}
	},

	logout : function() {	
		localStorage.removeItem("USER");
		window.location.href = '/login.html';
	},

	loadView : function(template, context, callback, reload) {
		var self = this;

		if ($('body').attr('id') == template && !reload) {
			return false;
		}

		$('body').attr('id', template);

		self.headerLoadOut(true);

		$.ajax({
			redirect : true,
			type: 'GET',
			url : 'views/'+template+'.html',
			beforeSend : function() {
				$('#main-content').append(self.loader);
			},
			success : function(data) {
				var template = Handlebars.compile(data);
				var html = template(context);

				$('#main-content').html(html);

				self.scrollToTop();

				if (typeof callback == 'function') {
					callback();
				}
			}	
		});
	},

	loadScript : function(src, lib, skipHistory) {
		lib = typeof lib == 'boolean' ? lib : false;
		var self = this;

		if (lib) {
			var scriptSource= 'scripts/lib/'+src+'.js';
			var script 		= document.createElement('script');
			script.type 	= 'text/javascript';
			script.src 		= scriptSource;

			if (!$('script[src="'+scriptSource+'"]').length) {
				$('body').append(script);
			}

			return true;
		}

		src = self.proccessGetParam(src);

		// if script is previously loaded
		var script = src.charAt(0).toUpperCase() + src.slice(1);
		if (window[script]) {
			var url = $.isEmptyObject(self.$_GET) ? '#'+src : self.buildGetParam(src);

			window[script].init();
			if (!skipHistory) {
				history.pushState({script: url}, null, url);
			}
			return true;
		}

		// load the script according to the hash
		var script 		= document.createElement('script');
		script.type 	= 'text/javascript';
		script.src 		= 'scripts/models/'+src+'.js';

		document.getElementsByTagName("head")[0].appendChild(script);


		if ($.isEmptyObject(history.state)) {
			var url = $.isEmptyObject(self.$_GET) ? '#'+src : self.buildGetParam(src);
			history.pushState({script: url}, null, url);
		} else {
			var url = $.isEmptyObject(self.$_GET) ? '#'+src : self.buildGetParam(src);
			if (history.state.script !== url) {
				history.pushState({script: url}, null, url);
			}
		}
	},

	proccessGetParam : function(src) {
		var self = this;
		self.$_GET = {};

		src = src.split('?');
		if (!$.isEmptyObject(src[1])) {
			var tmp  = src[1].split('&');

			$.each(tmp, function(k, v) {
				var tempGet = v.split('=');
				// var tempGetArray = [];
				// tempGetArray[tempGet[0]] = tempGet[1];
				self.$_GET[tempGet[0]] = tempGet[1];
			});
		} else {
			self.$_GET = {};
		}

		return src[0];
	},

	buildGetParam : function(src) {
		var self = this;
		var temp = '';

		$.each(self.$_GET, function(k, v) {
			temp += '&' + k + '=' + v;
		})

		var url = '#' + src + '?' + temp.substr(1);

		return url;
	},

	registerPartials : function(partials) {
		var self = this;
		for (var i in partials) {
			self.registerPartial(partials[i]);
		}
	},

	registerPartial : function(partial) {
		var self = this;

		self.getPartial(partial, function(data) {
  			if (!$.isEmptyObject(data)) {
				Handlebars.registerPartial(partial, data);
  			}
		});
	},

	getPartial : function(partial, callback) {
		$.ajax({
			internal: true,
	    	type : 'GET',
	  		url : '/partials/'+partial+'.html',
	  		success : function(data) {
	  			callback(data);
	  		}
	  	});
	},

	onpopstate : function() {
		var self = this;
		window.onpopstate = function(event) {
			var script = event.state ? event.state.script.substr(1) : '';
			if (!$.isEmptyObject(script)) {
				self.loadScript(script, false, true);
			}
		}
	},

	ajaxGlobalSetup : function() {
		var self = this;

		$.ajaxSetup({
			error : self.loadError()
		});
	},

	ajaxSend : function() {
		$(document).ajaxSend(function(event, jqxhr, settings) {
			if (isset(settings.redirect) || isset(settings.internal)) {
				return;
			}

			if (settings.type == 'GET') {
				var tmp = settings.url.split('?');
				var conjunctor = tmp.length > 1 ? '&' : '?';
				settings.url += conjunctor  + '_id=' + App.user.id + '&session_id=' + App.user.session_id;
				return;
			}

			if (!isset(settings.data)) {
				settings.data = '';
			}

			var conjunctor = !$.isEmptyObject(settings.data) ? '&' : '';
			settings.data += conjunctor + '_id=' + App.user.id + '&session_id=' + App.user.session_id;
		});
	},

	ajaxSuccess : function() {
		$( document ).ajaxSuccess(function( event, xhr, settings ) {
			if (isset(settings.redirect) || isset(settings.internal)) {
				return;
			}

			var response = JSON.parse(xhr.responseText);
			if (response.error) {
				alert(response.message);
			}
		});
	},

	handlebarsHelpersInit : function() {
		Handlebars.registerHelper('nl2br', function(options) {
			return new Handlebars.SafeString(
				options.fn(this).replace(new RegExp('\r?\n','g'), '<br />')
			);
		});		

		Handlebars.registerHelper('ifEqual', function(val1, val2, options) {
			if (val1 === val2) {
				return options.fn(this);
			} else {
				return options.inverse(this);
			}
		});	

		Handlebars.registerHelper('arrayIntersect', function(context1, context2, placeholder1, placeholder2, options) {
			var result = {};
			result[placeholder1] = [];

			for(var i=0, j=context1.length; i<j; i++) {
				var newContext = context1[i];

				if (context2.hasOwnProperty(context1[i].id)) {
					newContext[placeholder2] = context2[context1[i].id];
				}

				result[placeholder1].push(newContext);
			}
			
			return options.fn(result);
		});
	},

	scrollToTop : function() {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	},

	headerLoadOut : function(showHeader) {
		if (!showHeader) {
			return $('.header-wrapper').hide();
		}

		$('.header-wrapper').show();
	},

	loader : function(text) {
		var loaderText = !$.isEmptyObject(text) ? text : 'Loading...';

		return '<div class="row"><div class="col-xs-12 loader-wrapper"><div class="loader">'+loaderText+'</div></div></div>';
	},

	loadError : function(e) {
		var self = this;

		return function(e) {
			var context = {
				msg : 'Unable to load things properly.'
			};

			if (!e.status) {
				context.msg = 'No internet connection.'
			}

			self.loadView('error', context);
		}
	}

};

/* Helper functions */
var isset = function(obj) {
	if (typeof obj !== 'undefined') {
		return true;
	}

	return false;
}

var inArray = function(needle, haystack) {
	var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}	

var validEmail  = function(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$(document).ready(function() {
	App.init();
});